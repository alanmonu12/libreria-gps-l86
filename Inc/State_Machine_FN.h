/*
 * State_Machine_FN.h
 *
 *  Created on: 13/07/2018
 *      Author: equipo
 */

#ifndef STATE_MACHINE_FN_H_
#define STATE_MACHINE_FN_H_

void GPS_Init(void);
void GPS_Wait(void);
void GPS_Conect(void);
void GPS_get_status(void);
void GPS_Disconect(void);
void GPS_Standby(void);

#endif /* STATE_MACHINE_FN_H_ */
