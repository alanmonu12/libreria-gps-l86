/*
 * L86.h
 *
 *  Created on: 22/06/2018
 *      Author: equipo
 */

#ifndef L86_H_
#define L86_H_

#include <stdbool.h>
#include "NMEA.h"

#define L86_INIT_STRING_SIZE 74

/**
 * @brief Tama�o del buffer para almacenar todos los datos que se reciven por la UART
 * provenientes del m�dulo L86
 */
#define L86_BUFFER_SIZE 1024

/**
 * @brief UARTx con la cual se comunicar� con el m�dulo
 */
#define L86_UART &huart1

/**
 * @brief
 */
#define L86_DMA_UART  &hdma_usart1_rx

/**
 * @brief Funci�n para recivir por UARTx con DMA para los datos del m�dulo
 */
#define __L86_INIT_RECIEVE(__HANDLER,__BUFFER,__BUFFER_SIZE) HAL_UART_Receive_IT(__HANDLER,(uint8_t*)__BUFFER, __BUFFER_SIZE)

/**
 * @brief Comando para deshabilitar los mensajes con el fprmatp GPTXT
 * que env�a el m�dulo
 */
#define CDM_PQTXT_GPTXT "$PQTXT,W,0,1*23\r\n"

/**
 * @brief Comando para hacer que el m�dulo entre en modo standby
 */
#define CMD_PMTK_STANDBY "$PMTK161,0*28\r\n"
#define CMD_PMTK_STANDBY_SIZE 15

/**
 * @brief Buffer para almacenar la informaci�n recibida del m�dulo por medio de
 * UART
 */
char L86_Buffer[L86_BUFFER_SIZE];


typedef struct
{
	bool RMC_OUT;
	bool VTG_OUT;
	bool GGA_OUT;
	bool GSA_OUT;
	bool GSV_OUT;
	bool GLL_OUT;
}L86_OUT;

typedef enum
{
  L86_OK       = 0x00U,
  L86_ERROR    = 0x01U,
  L86_BUSY     = 0x02U,
  L86_TIMEOUT  = 0x03U
} L86_StatusTypeDef;


/**
  * @brief	Funci�n para inicializar el m�dulo, le indica que sentencias son
  * necesarias que env�e y cuales no. Adem�s inicia la primera recepci�n de la
  * UARTx.
  */
L86_StatusTypeDef L86_Init(void);

/**
  * @brief	Funci�n para poner el m�dulo en modo standby con el env�o del
  * 		comando $PMTK161,0*28\r\n.
  * @retval L86_StatusTypeDef Si el comando es env�ado correctamente regresa
  * 		L86_OK.
  */
L86_StatusTypeDef L86_standby(void);

/**
  * @brief	Funci�n para seleccionar el tipo de cadenas de NMEA 0183 que el
  * 		m�dulo va a enviar por UART. El comando es
  * 		$PMTK314,x,x,x,x,x,x,0,0,0,0,0,0,0,0,0,0,0,0,0*10\r\n
  * 		Las primeras 6 posiciones indican el tipo de cadena, si es un 0 no
  * 		enviar� esa cadena, por el contrario, si es un 1 enviar� la cadena.
  *
  * 		1- GLL
  * 		2- RMC
  * 		3- VTG
  * 		4- GGA
  * 		5- GSA
  * 		6- GSV
  *
  * @param  L86_OUT: Apuntador a una estructura que contiene los datos para
  * 		determinar la configuraci�n de las sentencias de salida.
  */
void L86_set_NMEA_out(L86_OUT* OUT_SETENCE);

/**
  * @brief	Funci�n hacer que el m�dulo salga del modo de standby, enviando una
  * 		cadena de caracteres por la UART.
  * @param  RMC_sentence: Apuntador a la cadena de caracteres con el formato GGA
  * 		de donde se desea obtener la informaci�n.
  * @param  RMC_data: Un apuntador a una variable del tipo NMEA_GGA_DATA que es
  * 		una estructura en donde se almacenar� la informaci�n obtenida de la
  * 		cadena de caracteres.
  * @retval bool Si ocurre alg�n error durante el proceso la funciona
  * 		regresa "false"
  */
L86_StatusTypeDef L86_wake_up(void);

L86_StatusTypeDef L86_hot_start(void);
L86_StatusTypeDef L86_periodic_mode(uint32_t time,
									uint32_t sleep_time,
									uint32_t second_run_time,
									uint32_t second_sleep_time,
									uint8_t type);

#endif /* L86_H_ */
