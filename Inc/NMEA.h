
#ifndef NMEA_H_
#define NMEA_H_

#include <stdbool.h>
#include "string.h"
#include "stdlib.h"
#include "stdint.h"

#define __NMEA_SENTENCE_CLEAR(SENTENCE,SIZE) memset(SENTENCE,'\0',SIZE)
#define __NMEA_CHECK_SENTENCE(SENTECE) if(*SENTECE != '$')return L86_ERROR
#define __NMEA_CHECKSUM(data, checksum)	nmea0183_checksum(data, checksum)

#define NMEA_BUFFER_COLUMN_SIZE 100
#define NMEA_BUFFER_ROW_SIZE 15


/**
 * @brief Buffer para almacenar las cadenas de string con el formato
 * NMEA 0183 con quince filas y cien columnas
 */
char NMEA_buffer[NMEA_BUFFER_ROW_SIZE][NMEA_BUFFER_COLUMN_SIZE];

/**
 * @brief Enumeraci�n para los parametros incluidos en un sentencia con el
 * formato RMC
 */
enum RMC_POSITION
{
	/*!< En la posici�n n�mero uno se encuentra la hora en formato hhmmss*/
	TIME_POSITION = 1,
	/*!< En la posici�n n�mero nueve se encuentra la fecha en formato ddmmyyyy*/
	DATE_POSITION = 9,
	/*!< En la posici�n n�mero siete se encuentra la velocidad en nudos*/
	SPEED_POSITION = 7
};

/**
 * @brief Enumeraci�n para los parametros incluidos en una sentencia con el
 * formato GGA
 */
enum GGA_POSITION
{
	/*!< En la posici�n n�mero dos se encuetra la latitud en grados*/
	LATITUDE_POSITION = 2,
	/*!< En la posici�n n�mero tres se encuetra la direcci�n Norte o Sur*/
	NS_POSITION = 3,
	/*!< En la posici�n n�mero cuatro se encuetra la longitud en grados*/
	LONGITUDE_POSITION = 4,
	/*!< En la posici�n n�mero cinco se encuetra la direcci�n Este u Oeste*/
	EW_POSITION = 5,
	/*!< En la posici�n n�mero nueve se encuetra la altitud*/
	ALTITUDE_POSITION = 9,
	/*!< En la posici�n n�mero seis se encuentra el estado actual del GPS*/
	STATUS_POSITION = 6
};

/**
 * @brief Enumeraci�n para los parametros incluidos en un sentencia con el
 * formato GSV
 */
enum GSV_POSITION
{
	/*!< En la posici�n n�mero tres se encuentra el n�mero de satelites en rango
	 * visible para el GPS*/
	SATELLITES_POSITION = 3
};


/**
  * @brief Definici�n de la estrucutura para almacenar el tiempo en el formato
  * hh/mm/ss.
  */
typedef struct
{
	/*!< Cadena de dos caracteres para guardar las horas hh */
	char hour[2];
	/*!< Cadena de dos caracteres para guardar los minutos mm */
	char minute[2];
	/*!< Cadena de dos caracteres para guardar los segundos ss */
	char second[2];
}NMEA_time;

/**
  * @brief Definici�n de la estrucutura para almacenar la fecha en el formato
  * dd-mm-yy.
  */
typedef struct
{
	/*!< Cadena de dos caracteres para guardar el d�a dd */
	char day[2];
	/*!< Cadena de dos caracteres para guardar el mes mm */
	char month[2];
	/*!< Cadena de dos caracteres para guardar el a�o yy */
	char year[2];
}NMEA_date;

/**
  * @brief Definici�n de la estrucutura para almacenar toda la informac�n de un
  * sentencia con el formato RMC.
  */
typedef struct
{
	/*!< Estructura para el tiempo */
	NMEA_time time;
	/*!< Estructura para la fecha */
	NMEA_date date;
	/*!< Variable para almacenar la velocidad */
	char* speed;
}NMEA_RMC_DATA;

/**
  * @brief Definici�n de la estrucutura para almacenar toda la informac�n de un
  * sentencia con el formato GGA.
  */
typedef struct
{
	/*!< Variable para almacenar la longitud con el formato dddmm.mmmm */
	char longitude[10];
	/*!< Variable para almacenar la altitud */
	char altitude[10];
	/*!< Variable para almacenar la latitud con el formato ddmm.mmmm */
	char latitude[9];
	/*!< Variable para almacenar la direcci�n N/S */
	char NS[2];
	/*!< Variable para almacenar la direcci�n E/W */
	char EW[2];
	/*!< Variable para almacenar el estado del dispositivo */
	char status[2];
}NMEA_GGA_DATA;

/**
  * @brief Definici�n de la estrucutura para almacenar toda la informac�n de un
  * sentencia con el formato GSV.
  */
typedef struct
{
	/*!< Variable para almacenar el n�mero de satelites que tiene el GPS
	 * 	en rango */
	char satellites[2];

}NMEA_GSV_DATA;

/**
  * @brief  Funci�n para parsear cadenas de caracteres del estandar NMEA 0183
  * 		con el formato RMC
  * 		"$GPRMC,123519,A,4807.038,N,01131.000,E,022.4,084.4,230394,003.1,W*6A"
  * 		de esta cadena de caracteres se puede obtener la hora, fecha y
  * 		velocidad que env�a el dispositivo GPS.
  * @param  RMC_sentence: Apuntador a la cadena de caracteres con el formato RMC
  * 		de donde se desea obtener la informaci�n.
  * @param  RMC_data: Un apuntador a una variable del tipo NMEA_RMC_DATA que es
  * 		una estructura en donde se almacenar� la informaci�n obtenida de la
  * 		cadena de caracteres.
  * @retval bool Si ocurre alg�n error durante el proceso la funciona
  * 		regresa "false"
  */
bool NMEA_parse_RMC(char *RMC_sentence,NMEA_RMC_DATA *RMC_data);

/**
  * @brief	Funci�n para parsear cadenas de caracteres del estandar NMEA 0183
  * 		con el formato GGA
  * 		"$GPGGA,172814.0,3723.46587704,N,12202.26957864,W,2,6,1.2,18.893,M,-25.669,M,2.0,0031*4F"
  * 		de esta cadena de caracteres se puede obtener latitud, logitud,
  * 		altitud y el estado del dispositivo.
  * @param  RMC_sentence: Apuntador a la cadena de caracteres con el formato GGA
  * 		de donde se desea obtener la informaci�n.
  * @param  RMC_data: Un apuntador a una variable del tipo NMEA_GGA_DATA que es
  * 		una estructura en donde se almacenar� la informaci�n obtenida de la
  * 		cadena de caracteres.
  * @retval bool Si ocurre alg�n error durante el proceso la funciona
  * 		regresa "false"
  */
bool NMEA_parse_GGA(char *GGA_sentence,NMEA_GGA_DATA *GGA_data);

/**
  * @brief	Funci�n para parsear cadenas de caracteres del estandar NMEA 0183
  * 		con el formato GSV
  * 		"$GPGSV,2,1,08,01,40,083,46,02,17,308,41,12,07,344,39,14,22,228,45*75"
  * 		de esta cadena de caracteres se puede obtener el n�mero de satelites
  * 		que tiene en rango el GPS.
  * @param  RMC_sentence: Apuntador a la cadena de caracteres con el formato GSV
  * 		de donde se desea obtener la informaci�n.
  * @param  RMC_data: Un apuntador a una variable del tipo NMEA_GSV_DATA que es
  * 		una estructura en donde se almacenar� la informaci�n obtenida de la
  * 		cadena de caracteres.
  * @retval bool Si ocurre alg�n error durante el proceso la funciona
  * 		regresa "false"
  */
bool NMEA_parse_GSV(char *GSV_sentence,NMEA_GSV_DATA *GSV_data);

/**
  * @brief	EL m�dulo GPS envia todas las sentencias del protocolo NMEA 0183 en
  * 		una sola cadena de caracteres,
  * 		est� funci�n sirve para obtener el n�mero total de sentecias
  * 		enviadas por el GPS.
  * @param  NMEA_sentence: Apuntador a la cadena de caracteres donde se
  * 		encuentran todas las sentencias del protocolo NMEA 0183.
  * @retval uint8_t El n�mero de sentencias en la cadena de caracteres.
  */
uint8_t NMEA_get_size(char *NMEA_sentence);

/**
  * @brief	Est� funci�n recorre toda la cadena de caracteres enviada por el GPS
  * 		y va almacenando cada sentencia en un buffer.
  * @param  NMEA_sentence: Apuntador a la cadena de caracteres donde se
  * 		encuentran todas las sentencias del protocolo NMEA 0183.
  * @param  buffer_NMEA: Apuntador al buffer para almacenar las sentencias.
  * @retval bool Si ocurre alg�n error durante el proceso la funciona
  * 		regresa "false"
  */
bool NMEA_scan(char *NMEA_sentence,char *buffer_NMEA);

/**
  * @brief	Est� funci�n convierte la latitud en grados a decimales.
  * @param  Degrees_sentence: Apuntador al valor en grados.
  * @param  Direction: N/S.
  * @retval float Retorna un valor de tipo flotante que es igual a la
  * 		laitud en decimales
  */
float NMEA_latitude_todecimal(char* Degrees_sentece,char* Direction);

/**
  * @brief	Est� funci�n convierte la longitud en grados a decimales.
  * @param  Degrees_sentence: Apuntador al valor en grados.
  * @param  Direction: E/W.
  * @retval float Retorna un valor de tipo flotante que es igual a la longitud
  * 		en decimales
  */
float NMEA_longitude_todecimal(char* Deg_sentece,char* Direction);

/****************************
 *
 *
 *
 * */
void NMEA_UTC_GTM_convert(NMEA_RMC_DATA *RMC_data);

/**
  * @brief	Funci�n para calcular el checksum de un sentencia.
  * @param  nmea_data: Sentencia para calcular el checksum.
  * @param  crc: variable donde se almacenar� el resultado del calculo.
  * @retval
  */
void nmea0183_checksum(char *nmea_data,int* checksum);


#endif /* NMEA_H_ */

