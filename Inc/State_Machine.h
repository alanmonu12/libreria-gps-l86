/*
 * State_Machine.h
 *
 *  Created on: 10/07/2018
 *      Author: equipo
 */

#ifndef STATE_MACHINE_H_
#define STATE_MACHIN

/*****************************/
/*Se definen los estados posibles*/
typedef enum
{
	ST_INIT,
	ST_WAIT,
	ST_CONECT,
	ST_DATA,
	ST_DISCONECT,
	ST_STANDBY
}state_t;

/********************************/
/*Se defiene los eventos que pueden detonar un cambio de estado*/
typedef enum
{
	EV_RECIVE_DATA,
	EV_PROCESS_CMPTL,
	EV_STANDBY,
	EV_DATA_VALID,
	EV_DATA_NO_VALID,
	EV_WAKE_UP,
	EV_INIT_CMPTL,
	NO_EVENT
}event_t;


typedef struct
{
	state_t currState;
}stateMachine_t;

/************************************/
void StateMachineInit(stateMachine_t *stateMachine);
void StateMachine_RunIteration(stateMachine_t *stateMachine,event_t event);
const char* StateMachine_GetStateName(state_t state);
event_t StateMachine_GetEvent(stateMachine_t *stateMachine);
#endif /* STATE_MACHINE_H_ */
