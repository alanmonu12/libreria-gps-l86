/**
 * @mainpage Librer�a NMEA
 * @section intro Introducci�n
 *
 * La librer�a que se presenta en este documento tiene como funci�n
 * obtener los datos que contiene una cadena de caracteres con la estructura del protocolo NMEA 0183, dicho protocolo
 * es usado en equipos maritimos y GPS, ayuda en la comunicaci�n entre distintos dispositivos. Est� protocolo est� definido
 * por la organizaci�n estadounidense National Marine Electronics Association.
 *
 *
 */


/**
  * @file NMEA.c
  * @brief Librer�a para el manejo de cadenas de NMEA
  *
  *
  */

#include <stdbool.h>
#include "L86.h"
#include "main.h"
#include "stm32f1xx_hal.h"
#include "string.h"
#include "stdlib.h"
#include "NMEA.h"

#define DMA_RX_BUFFER_SIZE          600
#define L86_UART &huart1
#define L86_DMA  &hdma_usart1_rx


bool NMEA_parse_RMC(char *RMC_sentence,NMEA_RMC_DATA *RMC_data)
{
	// $GPRMC,081836,A,3751.65,S,14507.36,E,000.0,360.0,130998,011.3,E*62
	// Apuntadores para el manejo del inicio y el fin de los datos contenidos
	// en la cadena de caracteres.
	char* start;
	char* end;

	//El GPS puede mandar cadenas con distintos formatos, se debe revisar que
	//sea de NMEA, si no lo es el proceso se detiene.
	__NMEA_CHECK_SENTENCE(RMC_sentence);

	// Start apunta al inicio del cadena de caracteres
	start = RMC_sentence;
	for (int i = 0; i < TIME_POSITION;i++)
	{
		// Se recorre la cadena hasta encontrar el n�mero de caracteres ','
		// igual a la posici�n en la que se encuentra el dato que deseamos
		// encontrar.
		start = strchr(start + 1,',');
	}

	// Se copian los valores en la estructura que va a contener los datos
	memcpy(RMC_data->time.hour,start + 1,2);
	memcpy(RMC_data->time.minute,start + 3,2);
	memcpy(RMC_data->time.second,start + 5,2);

	// Se regresa el apuntador start al inicio de la cadena
	start = RMC_sentence;

	//El procedimiento se repite para cada dato que se desea obtener de la
	//cadena de caracteres
	for (int i = 0; i < DATE_POSITION;i++)
	{
		start = strchr(start + 1,',');
	}

	memcpy(RMC_data->date.day,start + 1,2);
	memcpy(RMC_data->date.month,start + 3,2);
	memcpy(RMC_data->date.year,start + 5,2);

	start = RMC_sentence;
	for (int i = 0; i < SPEED_POSITION;i++)
	{
		start = strchr(start + 1,',');
	}

	end = strchr(start + 1,',');
	int size = (end - start)-1;
	char tmp[size];
	memcpy(tmp,start + 1,size);
	RMC_data->speed = tmp;

	return true;
}



bool NMEA_parse_GGA(char *GGA_sentence,NMEA_GGA_DATA *GGA_data)
{
   //"$GPGGA,123204.00,5106.94086,N,01701.51680,E,1,06,3.86,127.9,M,40.5,M,,*51"
	bool status = true;

   // Apuntadores para el manejo del inicio y el fin de los datos contenidos
   // en la cadena de caracteres.
	char* start;
	char* end;

	//Variable para almacenar la longitud de una cadena de caracteres
	uint8_t size;

	//El GPS puede mandar cadenas con distintos formatos, se debe revisar que
	//sea de NMEA, si no lo es el proceso se detiene.
	__NMEA_CHECK_SENTENCE(GGA_sentence);

	// Start apunta al inicio del cadena de caracteres
	start = GGA_sentence;
	for (int i = 0; i < LATITUDE_POSITION;i++)
	{
		// Se recorre la cadena hasta encontrar el n�mero de caracteres ','
		// igual a la posici�n en la que se encuentra el dato que deseamos
		// encontrar.
		start = strchr(start + 1,',');
	}

	//buscamos el final del dato, se logra buscando del inicio hasta el
	//siguiente caracter ',' en la cadena y end apunta a esa posici�n
	end = strchr(start + 1,',');

	//La longitud del dato es el espacio entre esos dos apuntadores menos 1
	size = (end - start) - 1;

	//La longitud de dato debe ser mayor a 0 ya que si no lo es quiere decir
	//que el GPS no esta conectado a la red de satelites
	if(size != false)
	{
		//Si estamos recibiendo informaci�n del GPS se limpian las variables
		//en donde se almacenan los datos
		__NMEA_SENTENCE_CLEAR(GGA_data->latitude,15);

		//Se copia la informaci�n a la estructura que contendra la informaci�n
		strncpy(GGA_data->latitude,start + 1,size);
	}
	else
	{
		__NMEA_SENTENCE_CLEAR(GGA_data->latitude,15);
	}

	/************************************/
	//El proceso es el mismo para todos los datos que se requieren
	start = GGA_sentence;
	for (int i = 0; i < NS_POSITION;i++)
	{
		start = strchr(start + 1,',');
	}
	end = strchr(start + 1,',');
	size = (end - start) - 1;

	if(size != false)
	{
		__NMEA_SENTENCE_CLEAR(GGA_data->NS,2);
		strncpy(GGA_data->NS,start + 1,size);
	}
	else
	{
		__NMEA_SENTENCE_CLEAR(GGA_data->NS,2);
	}


	/************************************/
	start = GGA_sentence;
	for (int i = 0; i < LONGITUDE_POSITION;i++)
	{
		start = strchr(start + 1,',');
	}
	end = strchr(start + 1,',');
	size = (end - start) - 1;

	if(size != false)
	{
		__NMEA_SENTENCE_CLEAR(GGA_data->longitude,15);
		strncpy(GGA_data->longitude,start + 1,size);
	}
	else
	{
		__NMEA_SENTENCE_CLEAR(GGA_data->longitude,15);
	}
	/***************************************************/
	start = GGA_sentence;
	for (int i = 0; i < EW_POSITION;i++)
	{
		start = strchr(start + 1,',');
	}
	end = strchr(start + 1,',');
	size = (end - start) - 1;

	if(size != false)
	{
		__NMEA_SENTENCE_CLEAR(GGA_data->EW,2);
		strncpy(GGA_data->EW,start + 1,size);
	}
	else
	{
		__NMEA_SENTENCE_CLEAR(GGA_data->EW,2);
	}

	/***************************************************/
	start = GGA_sentence;
	for (int i = 0; i < ALTITUDE_POSITION;i++)
	{
		start = strchr(start + 1,',');
	}
	end = strchr(start + 1,',');
	size = (end - start) - 1;

	if(size != false)
	{
		__NMEA_SENTENCE_CLEAR(GGA_data->altitude,15);
		strncpy(GGA_data->altitude,start + 1,size);
	}
	else
	{
		memset(GGA_data->altitude,'\0',15);
	}

	/**************************************************/
	start = GGA_sentence;
	for (int i = 0; i < STATUS_POSITION;i++)
	{
		start = strchr(start + 1,',');
	}
	end = strchr(start + 1,',');
	size = (end - start) - 1;

	if(size != false)
	{
		__NMEA_SENTENCE_CLEAR(GGA_data->status,2);
		strncpy(GGA_data->status,start + 1,size);
	}
	else
	{
		__NMEA_SENTENCE_CLEAR(GGA_data->status,2);
	}

	return status;

}

bool NMEA_parse_GSV(char *GSV_sentence,NMEA_GSV_DATA *GSV_data)
{
	// Apuntadores para el manejo del inicio y el fin de los datos contenidos
	// en la cadena de caracteres.
	char* start;
	char* end;

	//Variable para almacenar la longitud de una cadena de caracteres
	uint8_t size;

	//El GPS puede mandar cadenas con distintos formatos, se debe revisar que
	//sea de NMEA, si no lo es el proceso se detiene.
	__NMEA_CHECK_SENTENCE(GSV_sentence);
	/***********************************/
	start = GSV_sentence;
	for (int i = 0; i < SATELLITES_POSITION;i++)
	{
		// Se recorre la cadena hasta encontrar el n�mero de caracteres ','
		// igual a la posici�n en la que se encuentra el dato que deseamos
		// encontrar.
		start = strchr(start + 1,',');
	}

	//buscamos el final del dato, se logra buscando del inicio hasta el
	//siguiente caracter ',' en la cadena y end apunta a esa posici�n
	end = strchr(start + 1,',');

	//La longitud del dato es el espacio entre esos dos apuntadores menos 1
	size = (end - start) - 1;

	//La longitud de dato debe ser mayor a 0 ya que si no lo es quiere decir
	//que el GPS no esta conectado a la red de satelites
	if(size != false)
	{
		//Si estamos recibiendo informaci�n del GPS se limpian las variables
		//en donde se almacenan los datos
		__NMEA_SENTENCE_CLEAR(GSV_data->satellites,2);

		//Se copia la informaci�n a la estructura que contendra la informaci�n
		strncpy(GSV_data->satellites,start + 1,size);
		return true;
	}
	else
	{
		__NMEA_SENTENCE_CLEAR(GSV_data->satellites,15);
		return false;
	}


}

/******************************************************************************/
float NMEA_latitude_todecimal(char* Deg_sentece,char* Direction)
{
	//Cadena para guardar los grados
	char Deg[2];

	//Cadena para guardar los minutos
	char Min[7];

	//Variables flotantes para almacenar el resultado de la conversi�n
	float Deg_f;
	float Min_f;

	//Se copian los valores de grados y minutos a las variables para poder
	//trabajar por separado
	strncpy(Deg,Deg_sentece,2);
	strncpy(Min,Deg_sentece + 2,7);

	//Se convierten de caracteres a decimales
	Deg_f = atof(Deg);
	Min_f = atof(Min);

	//Se hace la conversi�n de grados a decimales, a los grados se le suma
	//los minutos dividos entre 60
	float Decimal = Deg_f + (Min_f/60);

	//Si la direcci�n obtenida del GPS es South 'S' se debe multiplicar los
	// grados decimales por -1 para obtener la direcci�n correcta
	if(*Direction == 'S')
	{
		Decimal *= - 1;
	}

	return Decimal;
}

float NMEA_longitude_todecimal(char* Deg_sentece,char* Direction)
{
	//Cadena para guardar los grados
	char Deg[3];

	//Cadena para guardar los minutos
	char Min[7];

	//Variables flotantes para almacenar el resultado de la conversi�n
	float Deg_f;
	float Min_f;

	//Se copian los valores de grados y minutos a las variables para poder
	//trabajar por separado
	strncpy(Deg,Deg_sentece,3);
	strncpy(Min,Deg_sentece + 3,7);

	//Se convierten de caracteres a decimales
	Deg_f = atof(Deg);
	Min_f = atof(Min);

	//Se hace la conversi�n de grados a decimales, a los grados se le suma
	//los minutos dividos entre 60
	float Decimal = Deg_f + (Min_f/60);

	//Si la direcci�n obtenida del GPS es West 'W' se debe multiplicar los
	// grados decimales por -1 para obtener la direcci�n correcta
	if(*Direction == 'W')
	{
		Decimal *= - 1;
	}

	return Decimal;
}

uint8_t NMEA_get_size(char *NMEA_sentence)
{
	uint8_t size = 0;

	// Apuntadores para el manejo del inicio y el fin de los datos contenidos
	// en la cadena de caracteres.
	char* start;
	char* end;

	//Start apunta al inicio de la cadena de caracteres
	start = NMEA_sentence;

	//Recorremos la cadena un n�mero mayor de veces que el n�mero de cadenas
	//con el formaro NMEA 0183 que esperamos recibir
	for(uint8_t i = 1; i < 500; i++)
	{
		//Para encontrar el final de una mensaje con el formato NMEA 0183 es
		//necesario buscar un caracter '\n', hacemos que el apuntador end
		//apunte a ese caracter, que es el final de la cadena
		end = strchr(start,'\n');

		//Star apunta al elemento siguiente de end para estar apuntado al inicio
		//de la siguiente cadena
		start = end + 1;

		//Cada vez que un ciclo se cumple, aumentamos la variable size
		size = i;

			//Si end esta apuntando al ultimo carcater '\n' dentro de la cadena,
			//entonces el loop termina
			if(end == strrchr(NMEA_sentence,'\n'))
			{
				break;
			}
	}
	// NOTA: Se puede realizar con un while lol

	//Se returna el valor de size que es el n�mero de cadenas con el formato
	//NMEA 0183 que se contaron
	return size;
}


bool NMEA_scan(char *NMEA_sentence,char *buffer_NMEA)
{
	// Apuntadores para el manejo del inicio y el fin de los datos contenidos
	// en la cadena de caracteres.
	char* NMEA_end;
	char* NMEA_start;

	//Revisar que el contenido de la cadena tenga el formato NMEA 0183
	__NMEA_CHECK_SENTENCE(NMEA_sentence);

	//NMEA_start apunta al inicio de la cadena de caracteres
	NMEA_start = NMEA_sentence;

	//El n�mero de cadenas con el formato 0183 contenidas debe ser mayor a 0
	if(NMEA_get_size(NMEA_sentence) != false)
	{
		//Recorremos buffer_NMEA asignado cada cadena con el formato 0183
		for(uint8_t Row_number = 0;Row_number < NMEA_get_size(NMEA_sentence);Row_number++)
		{
			//Buscamos el caracter '\n' que es el final de la cadena
			NMEA_end = strchr(NMEA_start,'\n');

			//Se copia la cadena al buffer
			memcpy(buffer_NMEA + (Row_number*NMEA_BUFFER_COLUMN_SIZE),NMEA_start,(NMEA_end - NMEA_start) + 1);

			//NMEA_star apunta al inicio de la siguiente cadena
			NMEA_start = NMEA_end + 1;
		}
	}
	else
	{
		return L86_ERROR;
	}

	//Se limpia el buffer donde se tiene la cadena de caracteres
	__NMEA_SENTENCE_CLEAR(NMEA_sentence,DMA_RX_BUFFER_SIZE);
	return L86_OK;
}


//void NMEA_UTC_GTM_convert(NMEA_RMC_DATA *RMC_data)
//{
//	char hour[2];
//
//	sprintf(hour,"%d",atoi(RMC_data->time.hour) - GTM_5);
//
//	if(atoi(RMC_data->time.hour) < 5)
//	{
//
//	}
//}

void nmea0183_checksum(char *nmea_data,int* checksum)
{
    int i;

    for (i = 0; i < strlen(nmea_data); i ++) {
        *checksum ^= nmea_data[i];
    }
}



