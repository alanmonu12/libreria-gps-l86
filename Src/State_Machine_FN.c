/*
 * State_Machine_FN.c
 *
 *  Created on: 13/07/2018
 *      Author: equipo
 */
#include "NMEA.h"
#include "main.h"
#include "stm32f1xx_hal.h"
#include "L86.h"

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern bool Init_cmplt;
extern bool Reiceve;
extern bool Status;
//extern bool Standby;
extern bool Data;
uint8_t counter = 0;

void GPS_Init()
{
	HAL_UART_Transmit(&huart2,(uint8_t*)"inicializacion\r\n",16,100);
	L86_Init();
	Init_cmplt = 1;
}

void GPS_Wait()
{
	Init_cmplt = false;
	Status = false;
	Data = false;
}

void GPS_Conect()
{
	HAL_UART_Transmit(&huart2,(uint8_t*)"Conectado\r\n",11,100);

	NMEA_GSV_DATA frame3;
	NMEA_parse_GSV(NMEA_buffer[2],&frame3);
	HAL_UART_Transmit(&huart2,(uint8_t*)frame3.satellites,2,HAL_MAX_DELAY);
	HAL_UART_Transmit(&huart2,(uint8_t*)"\r\n",2,1000);

	NMEA_GGA_DATA frame2;
	NMEA_parse_GGA(NMEA_buffer[1],&frame2);
	char a[8];
	char b[8];
	float latitude_f = NMEA_latitude_todecimal(frame2.latitude,frame2.NS);
	float longitude_f = NMEA_longitude_todecimal(frame2.longitude,frame2.EW);
	sprintf(b,"%f",longitude_f);
	sprintf(a,"%f",latitude_f);
	HAL_UART_Transmit(&huart2,(uint8_t*)a,8,1000);
	HAL_UART_Transmit(&huart2,(uint8_t*)"\r\n",2,1000);
	HAL_UART_Transmit(&huart2,(uint8_t*)b,8,1000);
	HAL_UART_Transmit(&huart2,(uint8_t*)"\r\n",2,1000);
	HAL_UART_Transmit(&huart2,(uint8_t*)frame2.altitude,strlen(frame2.altitude),1000);
	HAL_UART_Transmit(&huart2,(uint8_t*)"\r\n",2,1000);

	NMEA_RMC_DATA frame;
	NMEA_parse_RMC(NMEA_buffer[0],&frame);
	char time[10];
	char date[10];
	char speed[10];
	sprintf(time,"%c%c:%c%c:%c%c\r\n",frame.time.hour[0],frame.time.hour[1],frame.time.minute[0],frame.time.minute[1],frame.time.second[0],frame.time.second[1]);
	sprintf(date,"%c%c/%c%c/%c%c\r\n",frame.date.day[0],frame.date.day[1],frame.date.month[0],frame.date.month[1],frame.date.year[0],frame.date.year[1]);
	memcpy(speed,frame.speed,4);
	HAL_UART_Transmit(&huart2,(uint8_t*)time,sizeof(time),1000);
	HAL_UART_Transmit(&huart2,(uint8_t*)date,sizeof(date),1000);
	HAL_UART_Transmit(&huart2,(uint8_t*)speed,4,1000);
	HAL_UART_Transmit(&huart2,(uint8_t*)"\r\n",2,1000);
	Data = true;
	Reiceve = false;
	memset(NMEA_buffer, '\0', sizeof (NMEA_buffer));
	memset(L86_Buffer,'\0',L86_BUFFER_SIZE);
	__HAL_UART_FLUSH_DRREGISTER(L86_UART);
	__L86_INIT_RECIEVE(L86_UART,L86_Buffer,L86_BUFFER_SIZE);
}

void GPS_get_status()
{
	NMEA_scan(L86_Buffer,(char*)NMEA_buffer);
	NMEA_GGA_DATA frame;
	NMEA_parse_GGA(NMEA_buffer[1],&frame);
	if(frame.status[0] == '1')
	{
		Status = true;
	}
	if(frame.status[0] == '0')
	{
		Status = false;
	}

}

void GPS_Disconect()
{
	HAL_UART_Transmit(&huart2,(uint8_t*)"No conectado\r\n",14,100);
	Data = true;
	Reiceve = false;
	memset(NMEA_buffer, '\0', sizeof (NMEA_buffer));
	memset(L86_Buffer,'\0',L86_BUFFER_SIZE);
	__HAL_UART_FLUSH_DRREGISTER(L86_UART);
	__L86_INIT_RECIEVE(L86_UART,L86_Buffer,L86_BUFFER_SIZE);
}

void GPS_Standby()
{
	HAL_UART_Transmit(&huart2,(uint8_t*)"Standby mode",12,100);
	L86_standby();
}


