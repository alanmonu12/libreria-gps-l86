/*
 * State_Machine.c
 *
 *  Created on: 10/07/2018
 *      Author: equipo
 */
#include "State_Machine.h"
#include "State_Machine_FN.h"
#include "stdbool.h"

extern bool Init_cmplt;
extern bool Reiceve;
extern bool Status;
extern bool Standby;
extern bool Data;


/*Estructura para definir las transisciones entre estados */
typedef struct
{
	state_t currState;
	event_t event;
	state_t netxState;
}stateTransMatrixRow_t;
/***********************************/
/*Matriz de estados*/
static stateTransMatrixRow_t stateTransMatrix[] =
{
		  {ST_INIT,EV_INIT_CMPTL,ST_WAIT},
		  {ST_INIT,NO_EVENT,ST_INIT},
		  /*********************************/
		  {ST_WAIT,EV_RECIVE_DATA,ST_DATA},
		  {ST_WAIT,NO_EVENT,ST_WAIT},
		  /*********************************/
		  {ST_DATA,EV_DATA_VALID,ST_CONECT},
		  {ST_DATA,EV_DATA_NO_VALID,ST_DISCONECT},
		  /**********************************/
		  {ST_DATA,EV_STANDBY,ST_STANDBY},
		  {ST_WAIT,EV_STANDBY,ST_STANDBY},
		  {ST_CONECT,EV_STANDBY,ST_STANDBY},
		  {ST_DISCONECT,EV_STANDBY,ST_STANDBY},
		  /*********************************/
		  {ST_CONECT,EV_PROCESS_CMPTL,ST_WAIT},
		  /*********************************/
		  {ST_DISCONECT,EV_PROCESS_CMPTL,ST_WAIT},
		  /*********************************/
		  {ST_STANDBY,EV_STANDBY,ST_WAIT},
		  {ST_STANDBY,EV_WAKE_UP,ST_WAIT},
};

typedef struct
{
	const char* name;
	void (*func)(void);
}stateFunctionRow_t;

static stateFunctionRow_t stateFunctionA[] =
{
		{"ST_INIT", &GPS_Init},
		{"ST_WAIT", &GPS_Wait},
		{"ST_CONECT",&GPS_Conect},
		{"ST_DATA",&GPS_get_status},
		{"ST_DISCONECT",&GPS_Disconect},
		{"ST_STANDBY", &GPS_Standby},
};

void StateMachineInit(stateMachine_t *stateMachine)
{
	stateMachine->currState = ST_INIT;
}

event_t StateMachine_GetEvent(stateMachine_t *stateMachine)
{
	if(Init_cmplt == true && stateMachine->currState == ST_INIT)
	{
		return EV_INIT_CMPTL;
	}
	if(Reiceve == true && stateMachine->currState == ST_WAIT)
	{
		return EV_RECIVE_DATA;
	}
	if(Status == true && stateMachine->currState == ST_DATA && Standby == false)
	{
		return EV_DATA_VALID;
	}
	if(Status == false && stateMachine->currState == ST_DATA && Standby == false)
	{
		return EV_DATA_NO_VALID;
	}
	if(Standby == true && stateMachine->currState == ST_WAIT)
	{
		return EV_STANDBY;
	}
	if(Standby == false && stateMachine->currState == ST_STANDBY)
	{
		return EV_WAKE_UP;
	}
	if(Data == true && (stateMachine->currState == ST_CONECT || stateMachine->currState == ST_DISCONECT))
	{
		return EV_PROCESS_CMPTL;
	}

	return NO_EVENT;
}

void StateMachine_RunIteration(stateMachine_t *stateMachine,event_t event)
{
	for(int i = 0; i < sizeof(stateTransMatrix)/sizeof(stateTransMatrix[0]);i++)
	{
		if(stateTransMatrix[i].currState == stateMachine->currState)
		{
			if((stateTransMatrix[i].event == event) || (stateTransMatrix[i].event == NO_EVENT))
			{
				stateMachine->currState = stateTransMatrix[i].netxState;
				(stateFunctionA[stateMachine->currState].func)();
				break;
			}
		}

	}
}

const char * StateMachine_GetStateName(state_t state) {
    return stateFunctionA[state].name;
}
