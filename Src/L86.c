/*
 * L86.c
 *
 *  Created on: 22/06/2018
 *      Author: equipo
 */

#ifndef L86_C_
#define L86_C_

#include "L86.h"
#include "main.h"
#include "stm32f1xx_hal.h"
#include "string.h"
#include "stdlib.h"
#include <stdio.h>
#include "NMEA.h"


#define DMA_RX_BUFFER_SIZE          600
#define L86_UART &huart1
#define L86_DMA  &hdma_usart1_rx


extern UART_HandleTypeDef huart1;


L86_StatusTypeDef L86_Init()
{

	/*Se pone el pin de reset en un 1 para despertar el L86*/
	HAL_GPIO_WritePin(L86_RESET_GPIO_Port,L86_RESET_Pin,SET);
	/*Si no recibe los caracteres adecuados regresa un error y la m�quina de
	 * estados vuelve a entrar a la inicializac�n
	 * */
	memset(L86_Buffer,'\0',L86_BUFFER_SIZE);
	__HAL_UART_FLUSH_DRREGISTER(L86_UART);
	if(HAL_UART_Receive(L86_UART,L86_Buffer,L86_INIT_STRING_SIZE,5000) != HAL_OK)
	{
		return L86_ERROR;
	}
	/*Se declara el arreglo que representa las sentencias que son necesarias*/
	L86_OUT Sentence = {0,1,0,1,0,1};

	/*Se env�a el comando para configurar*/
	L86_set_NMEA_out(&Sentence);

	/*Se declara el arreglo que representa las sentencias que son necesarias*/
//	Sentence.VTG_OUT = 1;
//	Sentence.GSA_OUT = 1;
//	Sentence.GLL_OUT = 1;
//
//
//	/*Se env�a el comando para configurar*/
//	L86_set_NMEA_out(&Sentence);

	/*Se limpia el Data registrer del la UART*/
	__HAL_UART_FLUSH_DRREGISTER(L86_UART);

	memset(L86_Buffer,'\0',L86_BUFFER_SIZE);
	/*Se inicia la recepci�n por la UART*/
	__L86_INIT_RECIEVE(L86_UART,L86_Buffer,L86_BUFFER_SIZE);
	return L86_OK;
}

void L86_set_NMEA_out(L86_OUT* OUT_SETENCE)
{
	/*Un buffer para almacenar el comando a enviar*/
	char CMD_PMTK_NMEA_OUT[51];

	/*Variable para almacenar el checksum*/
	int checksum = 0;

	/*Se le da formato al comando con los valores necesarios y un checksum
	 * aleatorio*/
	sprintf(CMD_PMTK_NMEA_OUT,"PMTK314,%i,%i,%i,%i,%i,%i,0,0,0,0,0,0,0,0,0,0,0"
			",0,0",OUT_SETENCE->RMC_OUT,OUT_SETENCE->VTG_OUT,
			OUT_SETENCE->GGA_OUT,OUT_SETENCE->GSA_OUT,OUT_SETENCE->GSV_OUT,
			OUT_SETENCE->GLL_OUT);

	/*Se calcula el checksum correspodiente a los caracteres entre los
	 * simbolos de '$' y '*' */
	nmea0183_checksum(CMD_PMTK_NMEA_OUT,&checksum);

	/*Se agrega el checksum a la cadena*/
	sprintf(CMD_PMTK_NMEA_OUT,"$PMTK314,%i,%i,%i,%i,%i,%i,0,0,0,0,0,0,0,0,0,0,0"
			",0,0*%x\r\n",OUT_SETENCE->RMC_OUT,OUT_SETENCE->VTG_OUT,
			OUT_SETENCE->GGA_OUT,OUT_SETENCE->GSA_OUT,OUT_SETENCE->GSV_OUT,
			OUT_SETENCE->GLL_OUT,checksum);

	/*Se env�a el comando por medio de la UART*/
	HAL_UART_Transmit(L86_UART,(uint8_t*)CMD_PMTK_NMEA_OUT,strlen(CMD_PMTK_NMEA_OUT),1000);
}

L86_StatusTypeDef L86_standby(void)
{
	static char buffer[19];

	 HAL_UART_Transmit(L86_UART, (uint8_t *)CMD_PMTK_STANDBY,
	                      CMD_PMTK_STANDBY_SIZE, 100);
	 if(HAL_UART_Receive(L86_UART,(uint8_t*)buffer,19,100) != HAL_OK)
	 {
		 return L86_ERROR;
	 }
	 else
	 {
		 return L86_OK;
	 }
}


L86_StatusTypeDef L86_wake_up(void)
{
	static char buffer[74];
    HAL_UART_Transmit(L86_UART,(uint8_t*)"Wake",4,100);
	 if(HAL_UART_Receive(L86_UART,(uint8_t*)buffer,74,100) != HAL_OK)
	 {
		 return L86_ERROR;
	 }
	 else
	 {
		 return L86_OK;
	 }

}

L86_StatusTypeDef L86_periodic_mode(uint32_t run_time,
									uint32_t sleep_time,
									uint32_t second_run_time,
									uint32_t second_sleep_time,
									uint8_t type)
{
	char cmd[35];
	memset(cmd,'\0',35);
	int checksum;

	run_time *= 1000;
	sleep_time *= 1000;
	second_run_time *= 1000;
	second_sleep_time *= 1000;

	sprintf(cmd,"PMTK225,%d,%lu,%lu,%lu,%lu",type,run_time,sleep_time,
			second_run_time,second_sleep_time);

	nmea0183_checksum(cmd,&checksum);

	sprintf(cmd,"$PMTK225,%d,%lu,%lu,%lu,%lu*%x\r\n",type,run_time,sleep_time,
			second_run_time,second_sleep_time,checksum);

	HAL_UART_Transmit(L86_UART,(uint8_t*)cmd,strlen(cmd),100);

	return L86_OK;
}


L86_StatusTypeDef L86_hot_start(void)
{
	//char respuesta[37];
	//__HAL_UART_FLUSH_DRREGISTER(L86_UART);
	HAL_UART_Transmit(L86_UART,(uint8_t*)"$PMTK101*32\r\n",13,100);
	//HAL_UART_Receive(L86_UART,(uint8_t*)respuesta,37,1000);
	return L86_OK;
}

#endif /* L86_C_ */
